package com.company.V1;

import java.awt.*;
import java.util.Objects;
import java.util.Scanner;

public class Point3D {

    private double x,y,z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Point3D() {
        this (0,0,0);
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public double getZ(){
        return z;
    }
    public void setX(double x){
        this.x = x;
    }
    public void setY(double y){
        this.y = y;
    }
    public void setZ(double z){
        this.z = z;
    }
    public void print(Point3D point){
        System.out.println("(" + point.getX()+" "+ point.getY() + " " + point.getZ() + ")");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Math.abs(x-point3D.x)<=1e-9 &&
                Math.abs(y-point3D.y)<=1e-9 &&
                Math.abs(z-point3D.z)<=1e-9;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        Point3D point1 = new Point3D(2,4,6);
        Point3D point2 = new Point3D(2,4,6);
        Point3D point3 = new Point3D(a,b,c);
        if (point1.equals(point3)){
            System.out.println("Равны");
        } else {
            System.out.println("Не равны");
        }
        if (point1.equals(point1)){
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
