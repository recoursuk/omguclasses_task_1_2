package com.company.V1;

import java.util.Scanner;

public class SinTabulation {
    public static void main(String[] args) {

        double res;
        double changer;
        Scanner in = new Scanner(System.in);
        System.out.println("b, a, step. \nВведите шаг и пределы табулирования sin. Если будут введены неправильный формат значений они будут изменены.");
        double topPoint = in.nextDouble();
        double bottomPoint = in.nextDouble();
        double step = in.nextDouble();
        if(topPoint == bottomPoint){
            System.out.println("Вы ввели не диапозон");
            throw new Error();
        }
        if(step == 0){
            System.out.println("Шаг = 0 :(");
            throw new Error();
        }
        if (step<0){
            step*=-1;
        }
        if (topPoint < bottomPoint && step > 0){
            changer = bottomPoint;
            bottomPoint = topPoint;
            topPoint = changer;
        }

        for (double i = bottomPoint; i <= topPoint;i+=step){
            res = Math.sin(i);
            System.out.println(res+ " "+ i);
        }
    }
}
