package com.company.V1;

import java.util.Objects;

public class Vector3D {
    private double xEnd,yEnd,zEnd;
    public Vector3D(Point3D pointStart, Point3D pointEnd){
        this(pointEnd.getX() - pointStart.getX(),
            pointEnd.getY() - pointStart.getY(),
            pointEnd.getZ() - pointStart.getZ()
        );
    }
    public Vector3D(Vector3D vector){
        this.xEnd = vector.getXVector();
        this.yEnd = vector.getYVector();
        this.zEnd = vector.getZVector();
    }
    public Vector3D(double xEnd, double yEnd, double zEnd){
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        this.zEnd = zEnd;
    }
    public Vector3D(){
        this(0,0,0);
    }

    public double getXVector() {
        return xEnd;
    }

    public double getYVector() {
        return yEnd;
    }

    public double getZVector() {
        return zEnd;
    }

    public void setxEnd(double xEnd) {
        this.xEnd = xEnd;
    }

    public void setyEnd(double yEnd) {
        this.yEnd = yEnd;
    }

    public void setzEnd(double zEnd) {
        this.zEnd = zEnd;
    }

    public double length(){
        double xLength = (xEnd*xEnd);
        double yLength = (yEnd*yEnd);
        double zLength = (zEnd*zEnd);
        return  Math.sqrt(xLength+yLength+zLength);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3D vector3D = (Vector3D) o;
        return Double.compare(vector3D.xEnd, xEnd) == 0 &&
                Double.compare(vector3D.yEnd, yEnd) == 0 &&
                Double.compare(vector3D.zEnd, zEnd) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xEnd, yEnd, zEnd);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Vector3D{");
        sb.append("xEnd=").append(xEnd);
        sb.append(", yEnd=").append(yEnd);
        sb.append(", zEnd=").append(zEnd);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
        Point3D point1 = new Point3D(0,0,0);
        Point3D point2 = new Point3D(1,1,1);
        Vector3D vector1 = new Vector3D(point1,point2);
        System.out.println(vector1.length());
    }
}
