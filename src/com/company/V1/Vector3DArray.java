package com.company.V1;

public class Vector3DArray {
        private Vector3D[] array;

        public Vector3DArray(int size) throws Exception{
            if (size<=0) {
                throw new Exception("Размер не может быть меньше 0");
            }
            array = new Vector3D[size];
            for (int i = 0; i<size;i++){
                array[i] = new Vector3D();
            }
        }
        public int length(){
            return array.length;
        }

        public void addInIndex(Vector3D vector, int index) throws Exception{
            if (array == null){
                throw new Exception("Adding vector in null array");
            }
            if (index<0 || index>=array.length){
                throw new Exception("Index out of range");
            }
            array[index] = new Vector3D(vector);
        }
        public double maxLength() throws Exception{
            if (array == null){
                throw new Exception("Finding length of null array");
            }
            double max = -Double.MAX_VALUE;
            for (Vector3D elem:array){
                if (elem.length()>max){
                    max = elem.length();
                }
            }
            return max;
        }
        public int indexInArray(Vector3D vector) throws Exception{
            if(array == null){
                throw new Exception("Finding index in null array");
            }
            for(int i = 0;i<array.length;i++){
                if (vector.equals(array[i])){
                    return i;
                }
            }
            return -1;
        }
        public Vector3D arraySum() throws Exception{
            if(array == null){
                throw new Exception("Counting sum in null array");
            }
            Vector3D sum = new Vector3D();
            for(Vector3D elem:array){
                sum = new Vector3D(Vector3DProcessor.sum(sum,elem));
            }
            return sum;
        }

        public Vector3D linearCombination(double[] odds) throws Exception{
            if(array == null){
                throw new Exception("Attempt to count LinearCombination in null array");
            }
            if (odds.length != array.length){
                throw new  Exception("impossible to count linear combination with different size of arrays");
            }
            double x = 0, y = 0, z = 0;
            for (int i = 0; i < odds.length; i++){
                x+= array[i].getXVector()*odds[i];
                y+= array[i].getYVector()*odds[i];
                z+= array[i].getZVector()*odds[i];
            }
            Vector3D linearCombination = new Vector3D(x, y, z);
            return linearCombination;
        }

        public Point3D[] movePoints(Point3D point) throws Exception{
            if (array == null){
                throw new Exception("Null array");
            }
            Point3D arr[] = new Point3D[array.length];
            for(int i = 0; i<array.length; i++){
                arr[i] = new Point3D(
                        point.getX()+ array[i].getXVector(),
                        point.getY()+array[i].getYVector(),
                        point.getZ()+array[i].getZVector()
                );
            }
            return arr;
        }

}
