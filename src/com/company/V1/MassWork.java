package com.company.V1;

import java.util.Scanner;

public class MassWork {
    static Scanner in = new Scanner(System.in);
    static void fillArray(int[] array){
        for(int i=0;i<array.length;i++){
            array[i] = in.nextInt();
        }
    }
    static void printArray(int[] array){
        for(int elem:array){
            System.out.print(elem+" ");
        }
    }
    static int sum(int[] array){
        int summary=0;
        for (int elem:array){
            summary +=elem;
        }
        return summary;
    }
    static int even(int[] array){
        int count = 0;
        for (int elem:array){
            if (elem % 2 == 0){
                count++;
            }
        }
        return count;
    }
    static int range(int[] array, int a,int b){
        int count=0;
        for (int elem:array){
            if (elem>=a && elem<=b){
                count++;
            }
        }
        return count;
    }
    static boolean positive(int[] array){
        boolean positiveFlag = true;
        for (int elem:array){
            if (elem<=0){
                positiveFlag = false;
            }
        }
        return positiveFlag;
    }
    static void flip(int[] array){
        int changer;
        for (int i=0;i<array.length/2;i++){
            changer = array[i];
            array[i] = array[array.length-i-1];
            array[array.length-i-1] = changer;
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите кол-во элементов в массиве");
        int n = in.nextInt();
        System.out.println("Введите диапозон [a;b]");
        int a = in.nextInt();
        int b = in.nextInt();
        int[] array = new int[n];
        fillArray(array);
        printArray(array);
        System.out.println("Сумма всех элементов: "+sum(array));
        System.out.println("Количество четных элементов: "+even(array));
        System.out.println("Количество элементов входящих в [a;b]: "+range(array,a,b));
        positive(array);
        if (positive(array) == true){
            System.out.println("All positive");
        } else {
            System.out.println("Not all positive");
        }
        flip(array);
        printArray(array);
    }
}
