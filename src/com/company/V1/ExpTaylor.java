package com.company.V1;

import java.math.BigDecimal;
import java.util.Scanner;

public class ExpTaylor {
    public static BigDecimal TaylorSeries(double x, double acc) throws Exception {
        if (acc < 0) {
            throw new Exception("Accuracy is less then 0");
        }
        BigDecimal sum = BigDecimal.ONE, nextSum = BigDecimal.ONE;
        for (int i = 1; nextSum.abs().compareTo(new BigDecimal(acc)) >= 0; ++i) {
            nextSum = nextSum.multiply(new BigDecimal(x / i));
            sum = sum.add(nextSum);
        }
        return sum;

    }

    public static void main(String[] args) throws Exception {
//        Scanner in = new Scanner(System.in);
//        BigDecimal sum = BigDecimal.ONE, nextSum = BigDecimal.ONE;
//        double x = in.nextDouble();
        double accuracy = 1e-10;
        System.out.println(TaylorSeries(0, accuracy));
        System.out.println(TaylorSeries(-2, accuracy));
        System.out.println(TaylorSeries(10, accuracy));
        System.out.println(TaylorSeries(1000, accuracy));
    }
}
