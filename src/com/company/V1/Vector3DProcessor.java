package com.company.V1;

public class Vector3DProcessor {

    public static Vector3D sum(Vector3D vector1, Vector3D vector2){
        return new Vector3D(vector1.getXVector() + vector2.getXVector(),
                            vector1.getYVector()+vector2.getZVector(),
                            vector1.getZVector()+vector2.getZVector()
        );

    }

    public static Vector3D dif(Vector3D vector1, Vector3D vector2){
        return new Vector3D( vector1.getXVector() - vector2.getXVector(),
                vector1.getYVector()-vector2.getZVector(),
                vector1.getZVector()-vector2.getZVector()
        );
    }

    public static double skalarMul(Vector3D vector1, Vector3D vector2){
        return vector1.getXVector()*vector2.getXVector()+
                vector1.getYVector()*vector2.getYVector()+
                vector1.getZVector()*vector2.getZVector();
   }
   public  static Vector3D vectorMul(Vector3D vector1, Vector3D vector2){
        Point3D zeroPoint = new Point3D(0,0,0);
        return new Vector3D(zeroPoint, new Point3D(
                vector1.getYVector()*vector2.getZVector() - vector1.getZVector()*vector2.getYVector(),
                vector1.getXVector()*vector2.getZVector() - vector1.getZVector()*vector2.getXVector(),
                vector1.getXVector()*vector2.getYVector() - vector1.getYVector()*vector2.getXVector()
        ));
   }

   public static boolean isColl(Vector3D vector1, Vector3D vector2) throws Exception{

        if (vector1.equals(new Vector3D()) || vector2.equals(new Vector3D())){
            throw new Exception("Вектора не могут быть нулевыми");
        }
        return vectorMul(vector1, vector2).equals(new Vector3D());
   }
}
