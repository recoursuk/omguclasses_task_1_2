package com.company;

import java.util.Scanner;

public class Equation {
    public static void main(String[] args) {
        double dis,x1,x2 = 0;
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        double b = in.nextDouble();
        double c = in.nextDouble();
//        if (b == 0 && c == 0){
//            System.out.println("Вы ввели плохие значения(");
//            throw new Error();
//        }
        if (a==0){
            System.out.println("Это не квадратное уравнение))");
            throw new Error();
        }
        dis = b*b-4*a*c;
        if (dis>0){
            System.out.println("Корня 2:");
            x1 = (-b-Math.sqrt(dis))/2*a;
            x2 = (-b+Math.sqrt(dis))/2*a;
            System.out.println("x1 = "+x1+" x2 = "+x2);
        } else if (dis == 0){
            System.out.println("Корень один ");
            System.out.println("x = " + -b/2*a);
        } else {
            System.out.println("Корней нет");
        }
    }
}
