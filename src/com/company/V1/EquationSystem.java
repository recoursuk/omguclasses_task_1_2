package com.company.V1;

import java.util.Scanner;

public class EquationSystem {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);

//        double a = in.nextDouble();
//        double b = in.nextDouble();
//        double e = in.nextDouble();
//        double c = in.nextDouble();
//        double d = in.nextDouble();
//        double f = in.nextDouble();
        solveSystem(0,0,0, 0,0,0);
        System.out.println();
        solveSystem(0,0,1, 0,0,0);
        solveSystem(0,0,0, 0,0,6);
        solveSystem(1,2,3, 0,0,6);
        solveSystem(1,0,3, 1,0,6);
        solveSystem(0,1,3, 0,1,6);
        solveSystem(1,1,3, 3,3,6);
        System.out.println();
        solveSystem(1,1,3, 0,0,0);
        solveSystem(1,1,3, 2,2,6);
        solveSystem(0,1,3, 0,2,6);
        solveSystem(1,0,3, 2,0,6);
        System.out.println();
        solveSystem(1,2,3,4,5,6);

    }
    public static void solveSystem(double a, double b, double c, double d, double e, double f) throws Exception {
        double x = 0, y = 0;
        double delta = a*e - b*d;
        double deltaX = c*e - f*b;
        double deltaY = a*f - c*d;
        if ( (a == 0 && b == 0 && c!=0) || (d==0 && e ==0 && f !=0) || (a*d == c*b && a*f != c*e) || (a == d && b == e && c!=f)){
            System.out.println("Система не имеет решений");
        }
        else if (Double.compare(Math.abs(delta), Double.compare(Math.abs(deltaX), Math.abs(deltaY))) == 0){
            //throw new Exception("Система несовместна следовательна решений бесконечна многа или нету(((((");
            System.out.println("Система имеет бесконечно много решений");
        } else if (Math.abs(delta) <1e-9 && Math.abs(deltaX) > 0 && Math.abs(deltaY) > 0){
            System.out.println("Система не имеет решений");
        }
        else {
            x = deltaX / delta;
            y = deltaY / delta;
            System.out.println("x: " + x);
            System.out.println("y: " + y);
        }

    }
}
