package com.company.V2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringProcessor {
    public static String mulStr(String string, int n) throws Exception {
        if(string == null){
            throw new Exception("Can't multiply null string");
        }
        if(n<0) {
            throw new Exception("Can't multiply string negative times");
        }
        StringBuilder sb = new StringBuilder(string);
        for(int i = 0; i < n-1; i++){
            sb.append(string);
        }
        return sb.toString();
    }
    public static int countOfSubstring(String string1, String string2) throws Exception{
        if(string1 == null || string2 == null){
            throw new Exception("String can't be null");
        }
        int counter = 0;
        //qqqqq qq 2<5
        for(int i = 0;i + string2.length() <= string1.length();++i){
            if(string1.substring(i,i + string2.length()).contains(string2)){
                counter++;
            }
        }
        return counter;
    }
    public static String charToNumberReplace(String string)throws Exception{
        if(string == null){
            throw new Exception("String can't be null");
        }
        StringBuilder sb = new StringBuilder(string);
        for(int i = 0; i<sb.length(); i++){
            switch (sb.charAt(i)){
                case '1': {
                    sb.deleteCharAt(i);
                    sb.insert(i,"one");
                    break;
                }
                case '2': {
                    sb.deleteCharAt(i);
                    sb.insert(i,"two");
                    break;
                }
                case '3': {
                    sb.deleteCharAt(i);
                    sb.insert(i,"three");
                    break;
                }
            }
        }
        return sb.toString();
    }
    public static void charRemove(StringBuilder sb) throws Exception{
        if (sb == null){
            throw new Exception("String Builder can't be null");
        }

        for (int i = 1; i < sb.length(); i++){
            sb.deleteCharAt(i);
        }
    }
    public static StringBuilder swapWords(StringBuilder sb) throws Exception{
        if (sb == null){
            throw new Exception("String Builder can't be null");
        }
        int indexStart = 0, indexEnd = 0;
        while(sb.charAt(indexStart) == ' '){
            indexStart++;
        }
        int indexStartStops = indexStart;
        while(sb.charAt(indexStartStops) !=' '){
            indexStartStops++;
            if(indexStartStops == sb.length()){
                return sb;
            }
        }
        sb.reverse();
        while(sb.charAt(indexEnd) == ' '){
            indexEnd++;
        }
        int indexEndStops = indexEnd;
        while(sb.charAt(indexEndStops) != ' '){
            indexEndStops++;
        }
        sb.reverse();
        String word1 = sb.substring(indexStart,indexStartStops);
        String word2 = sb.substring(sb.length()-indexEndStops, sb.length() - indexEnd);
        sb.delete(indexStart,indexStartStops);
        sb.insert(indexStart, word2);
        sb.delete(sb.length()-indexEndStops, sb.length() - indexEnd);
        sb.insert(sb.length() - indexEnd,word1);
        return sb;
    }
    public static String hexToDec(String string) throws Exception{
        if (string == null || string.length() == 0){
            throw new Exception("String can't be null");
        }
        String[] arr = string.split(" ");
        int index = 0;
        //System.out.println(Integer.parseInt("0x123", 16));
        for (String elem:arr){
            if(elem.startsWith("0x")){
                try {
                    int value = Integer.decode(elem);
                    arr[index] = Integer.toString(value);
                } catch (Exception e){

                }
            }
            index++;
        }
        return String.join(" ",arr);
    }

}
