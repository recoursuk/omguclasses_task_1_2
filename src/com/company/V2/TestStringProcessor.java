package com.company.V2;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static com.company.V2.StringProcessor.mulStr;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestStringProcessor {

    @Test
    public void TestMulStr() throws Exception {
        String shortString = "1";
        String nullString = null;
        String longString = "11111111111111111111111111111111111111111111111111";
        Assert.assertEquals(mulStr(shortString,5).length(),5);
        Assert.assertEquals(mulStr(longString,5).length(),250);
        Assert.assertNotNull(assertThrows(Exception.class, () -> { mulStr(nullString,5); }).getMessage());
        Assert.assertNotNull(assertThrows(Exception.class, () -> { mulStr(shortString,-5); }).getMessage());

    }

    @Test
    public void TestCountOfSubstring() throws Exception {
        String shortString ="1";
        String nullString = null;
        String longString = "11111111111111111111111111111111111111111111111111";
        Assert.assertEquals(StringProcessor.countOfSubstring(longString,shortString),50);
        Assert.assertEquals(4, StringProcessor.countOfSubstring("qqqqq","qq"));
        Assert.assertEquals(StringProcessor.countOfSubstring(shortString,shortString),1);
        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.countOfSubstring(nullString,longString); }).getMessage());
        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.countOfSubstring(longString,nullString); }).getMessage());
    }

    @Test
    public void TestCharToNumberReplace() throws Exception {
        String nullString = null;
        Assert.assertEquals(StringProcessor.charToNumberReplace("1"),"one");
        Assert.assertEquals(StringProcessor.charToNumberReplace("2"),"two");
        Assert.assertEquals(StringProcessor.charToNumberReplace("3"),"three");
        Assert.assertEquals(StringProcessor.charToNumberReplace("13"),"onethree");
        Assert.assertEquals(StringProcessor.charToNumberReplace("a2aaaaa1"),"atwoaaaaaone");

        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.charToNumberReplace(null); }).getMessage());
    }

    @Test
    public void TestEvenCharRemove() throws Exception {
        StringBuilder string1 = new StringBuilder("1 1 1 1 1 1 1 1 ");
        StringBuilder string2 = new StringBuilder("1212121212121212");
        StringBuilder string3 = new StringBuilder("s");
        StringProcessor.charRemove(string1);
        Assert.assertEquals(string1.toString(),"11111111");
        StringProcessor.charRemove(string2);
        Assert.assertEquals(string2.toString(),"11111111");
        StringProcessor.charRemove(string3);
        Assert.assertEquals(string3.toString(),"s");

        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.charRemove(null); }).getMessage());

    }

    @Test
    public void TestSwapWords() throws Exception {
        StringBuilder string1 = new StringBuilder("");
        StringBuilder string2 = new StringBuilder(" word1 word2 word3");
        StringBuilder string3 = new StringBuilder("    word1 word2 word3   ");
        StringBuilder string4 = new StringBuilder(" woooooord1 word2 w");
        Assert.assertEquals(StringProcessor.swapWords(string2).toString()," word3 word2 word1");
        Assert.assertEquals(StringProcessor.swapWords(string3).toString(),"    word3 word2 word1   ");
        Assert.assertEquals(StringProcessor.swapWords(string4).toString()," w word2 woooooord1");

        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.swapWords(null); }).getMessage());
        Assert.assertNotNull(assertThrows(Exception.class, () -> { StringProcessor.swapWords(string1); }).getMessage());
    }
    @Test
    public void TestHexToDec() throws Exception {
        String string = "0x00000000";
        String string1 = "0x00000001";
        String string2 = "0x0000000g";
        String string3 = "0x00110000011";
        Assert.assertEquals(StringProcessor.hexToDec(string),"0");
        Assert.assertEquals(StringProcessor.hexToDec(string1),"1");
        Assert.assertEquals(StringProcessor.hexToDec(string2),"0x0000000g");
        Assert.assertEquals(StringProcessor.hexToDec(string3),"0x00110000011");
        String string4 = "0x00000000 DDDDD 0x00000001 0x00000002 0x00000003";
        Assert.assertEquals(StringProcessor.hexToDec(string4),"0 DDDDD 1 2 3");

    }
}
